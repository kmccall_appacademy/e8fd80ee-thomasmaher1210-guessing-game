# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.


  def guessing_game
    answer = 1 + rand(99)
    count = 0
    won = false

    until won
      puts "Guess a number 1-100"
      guess = gets.chomp.to_i

        if guess == answer
          puts "Your guess, #{guess} was correct."
          won = true
        elsif guess < answer
          puts "#{guess} is too low!"
        elsif guess > answer
          puts "#{guess} is too high!"
        else
          puts "Invalid"
        end

      count += 1
    end

    puts "it took you #{count} guesses"
  end


# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
